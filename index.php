<html>
    
    <head>
        <meta charset = "utf-8" name = "viewport" content ="width=device-width, initial-scale=1.0">
        <title>Louis Leslie</title>
        <link href = "static/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        
        <!-- Page content -->
        <div class="container">
            <?php
                require_once "route.php";
            ?>    
        </div>
        
        <!-- Bootstrap core JavaScript -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="static/js/jquery-slim.min.js"></script>
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/popper.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>
    </body>

</html>
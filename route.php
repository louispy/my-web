<?php

// Grabs the URI and breaks it apart in case we have querystring stuff
$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

// Routing
switch($request_uri[0]){

    // Home
    case '/':
        require 'view/home.php';
        break;
    // About
    case '/about':
        require 'view/about.php';
        break;
    // Contact
    case '/contact':
        require 'view/contact.php';
        break;
    // Articles
    case '/articles':
        require 'view/articles.php';
        break;
    // Bio
    case '/bio':
        require 'view/bio.php';
        break;
    // Else
    default:
        header('HTTP/1.0 404 Not Found');
        require 'view/404.php';
        break;
}

?>